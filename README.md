# PrimaryBid QA Test

## Running the code
- Clone the repository
- Run yarn to install all dependencies
- Set up three environment variables:
```
export CYPRESS_PB_AUTH_PASS=<URL OF TEST SITE>
export CYPRESS_PB_AUTH_PASS=<BASIC AUTH USERNAME>
export CYPRESS_PB_AUTH_PASS=<BASIC AUTH PASSWORD>
```
- Run `yarn cypress run` to run the tests

## Next steps
A couple of next steps that would make sense given more time:
- Having to enter the basic auth on each visit isn't good, we should overwrite the visit commands so this is handled for us
- The baseURl could be handled better, we should be setting a baseURL from the environment variable
- The `succesful signup goes to profile page` isn't great, I didn't quite understand the instructions as after completing the form you aren't taken to the user/profile page so not sure what was being tested here. This test could be improved with better understanding of what should happen.
- The `all content can be filtered for webinar` test could be improved to avoid having to use a `{force: true}`, the hovering on filter all seemed to be inconsistent, more time needs to be spent to look into this.

## UX/Other Issues
Things I noticed:
- When a filter is applied to All Content on /news the white 'Filter By' text on a green background is hard to read
- Hovering over 'Filter By' on /news initially shows none of the boxes checked, suggesting that none of the categories are being included, in reality they are all included
- (This might just because it's a playground site) None of the webinar links on the news page work
- If you enter a valid email and then an invalid password (or passwords that don't match), then the email field is also blanked, forcing you to enter it again
- Terms & Policies link on the signup page doesn't appear when you first go to the signup page, only when you make a mistake (I think only when you enter two passwords that don't match)
- The Terms & Policies link on the signup page doesn't have a 'click' cursor when you hover over it
