/// <reference types="cypress" />

context("Home Page", () => {
  beforeEach(() => {
    cy.visit(Cypress.env("PB_URL"), {
      auth: {
        username: Cypress.env("PB_AUTH_USER"),
        password: Cypress.env("PB_AUTH_PASS")
      }
    });
  });

  it("about us link navigates to about us page", () => {
    cy.contains("About Us").click();
    cy.url().should("equal", `${Cypress.env("PB_URL")}about`);
  });

  it("help link navigates to FAQ page", () => {
    cy.contains("Help").click();
    cy.url().should("equal", `${Cypress.env("PB_URL")}faqs`);
  });

  it("news link navigates to news page", () => {
    cy.contains("News").click();
    cy.url().should("equal", `${Cypress.env("PB_URL")}news`);
  });

  it("sign up button navigates to sign up page", () => {
    cy.contains("Sign up").click();
    cy.url().should("equal", `${Cypress.env("PB_URL")}user/signup/`);
  });
});
