/// <reference types="cypress" />
import faker from "faker";

context("Sign up Page", () => {
  beforeEach(() => {
    cy.visit(`${Cypress.env("PB_URL")}user/signup/`, {
      auth: {
        username: Cypress.env("PB_AUTH_USER"),
        password: Cypress.env("PB_AUTH_PASS")
      }
    });
  });

  it("succesful signup goes to profile page", () => {
    const email = `cypress_${new Date().getTime()}@example.com`;
    const password = faker.internet.password();

    cy.log(email);
    cy.log(password);

    cy.get('[name="email"]').type(email);
    cy.get('[name="password"]').type(password);
    cy.get('[name="confirmPassword"]').type(password);

    cy.contains("Submit").click();

    // don't end up on profile page as instructions described, completing futher
    // forms also doesn't redirect to profile page.

    cy.visit(`${Cypress.env("PB_URL")}user/profile/`, {
      auth: {
        username: Cypress.env("PB_AUTH_USER"),
        password: Cypress.env("PB_AUTH_PASS")
      }
    });

    cy.contains("Personal details").should("be.visible");
  });

  it("passwords must match", () => {
    const email = `cypress_${new Date().getTime()}@example.com`;
    const passwordOne = faker.internet.password();
    const passwordTwo = faker.internet.password();

    cy.get('[name="email"]').type(email);
    cy.get('[name="password"]').type(passwordOne);
    cy.get('[name="confirmPassword"]').type(passwordTwo);

    cy.contains("Submit").click();

    cy.contains("Passwords do not match").should("be.visible");
  });

  it("email address must be valid", () => {
    const email = "not an email";
    const password = faker.internet.password();

    cy.get('[name="email"]').type(email);
    cy.get('[name="password"]').type(password);
    cy.get('[name="confirmPassword"]').type(password);

    cy.contains("Submit").click();

    cy.contains("The email you have entered is not valid").should("be.visible");
  });
});
