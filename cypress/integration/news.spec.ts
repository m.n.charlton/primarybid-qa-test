/// <reference types="cypress" />

context("News Page", () => {
  beforeEach(() => {
    cy.visit(`${Cypress.env("PB_URL")}news`, {
      auth: {
        username: Cypress.env("PB_AUTH_USER"),
        password: Cypress.env("PB_AUTH_PASS")
      }
    });
  });

  it("news items for featured content exist", () => {
    // Get the container containing the Featured Content and make sure it contains
    // news items
    cy.contains("Featured Content")
      .parent()
      .next()
      .within(() => {
        cy.get("[data-testid='news-data']").should("exist");
      });
  });

  it("news items for all content exist", () => {
    // Get the container containing All Content and make sure it contains
    // news items
    cy.contains("All Content")
      .parent()
      .next()
      .within(() => {
        cy.get("[data-testid='news-data']").should("exist");
      });
  });

  it("all content can be filtered for webinar", () => {
    cy.get("[data-testid='news-filter-button']").trigger("mouseover");
    cy.wait(1000);
    cy.get('[data-testid="news-filter-checkbox-Webinar"]').within(() => {
      cy.get(".pb_news_filter__checkbox_iEUe").click({ force: true });
    });

    cy.contains("All Content")
      .parent()
      .next()
      .within(() => {
        cy.contains("Media Coverage").should("not.exist");
        cy.contains("Whitepaper").should("not.exist");
        cy.contains("Press Release").should("not.exist");
      });
  });
});
